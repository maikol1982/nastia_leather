from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.template import response


def index(request):
    return render(request, "leather_temps/paparies.html", [])
    # return HttpResponse("Hello, world. You're at the polls index.")
def photos(request):
    return render(request, "leather_temps/media_gallery.html", [])

def index_temp(request):
    return render(request, "leather_temps/index.html", [])