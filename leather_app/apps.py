from django.apps import AppConfig


class LeatherAppConfig(AppConfig):
    name = 'leather_app'
