from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'home', views.index, name='index'),
    url(r'photos', views.photos, name='photos'),
    url(r'index', views.index_temp, name='index_temp'),
    ]